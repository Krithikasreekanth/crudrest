package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.businesslogic.BankService;
import com.model.Bank;

@RestController
public class BankController {
	@Autowired
	private BankService bankService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createController(@RequestBody Bank bank) {
		String returnString = " ";
		boolean returnbool = bankService.createService(bank);
		if (returnbool == true) {
			returnString = "Success " + bank.getBankName();
		} else {
			returnString = "failure";
		}
		return returnString;
	}

	@RequestMapping(value = "/read/{bankId}", method = RequestMethod.GET)
	public Bank readController(@PathVariable int bankId) {
		return bankService.readService(bankId);
	}

	@RequestMapping(value = "/update/{bankCity}", method = RequestMethod.PUT)
	public String updateController(@PathVariable String bankCity) {
		return bankService.updateService(bankCity);
	}

	@RequestMapping(value = "/delete/{bankId}", method = RequestMethod.DELETE)
	public boolean deleteController(@PathVariable int bankId) {
		return bankService.deleteService(bankId);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Bank> listController(Bank bank) {
		return bankService.getAllBankService();
	}

}
