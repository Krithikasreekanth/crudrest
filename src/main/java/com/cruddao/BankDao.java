package com.cruddao;

import java.util.List;

import com.model.Bank;

public interface BankDao {
	public boolean create(Bank bank);

	public Bank read(int bankId);

	public String update(String bankCity);

	public boolean delete(int bankId);

	public List<Bank> getAllBank();
	

}
