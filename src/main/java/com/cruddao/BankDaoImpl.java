package com.cruddao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.model.Bank;

@Repository
public class BankDaoImpl implements BankDao {

	public boolean create(Bank bank) {
		boolean tempVariable = false;
		if (bank != null && bank.getBankName().length() > 0) {
			System.out.println("Bank id=" + bank.getBankId());
			System.out.println("Bank name=" + bank.getBankName());
			System.out.println("Bank city=" + bank.getBankCity());

			tempVariable = true;
		} else {
			tempVariable = false;
		}
		return tempVariable;

	}

	public Bank read(int bankId) {
		Bank bank = null;
		if (Integer.toString(bankId).length() > 0) {
			if (bankId == 10) {
				bank = new Bank(10, "IOB", "Chennai");
			}
			if (bankId == 20) {
				bank = new Bank(20, "KVB", "Madurai");

			}
		} else {
			System.out.println("enter correct value");
		}
		return bank;
	}

	public String update(String bankCity) {
		System.out.println("update");
		return null;
	}

	public boolean delete(int bankId) {
		System.out.println("delete");
		return false;
	}

	public List<Bank> getAllBank() {
		Bank bank1 = new Bank(10, "IOB", "Chennai");
		Bank bank2 = new Bank(20, "Axis", "Coimbatore");
		Bank bank3 = new Bank(30, "HDFC", "Madurai");
		List<Bank> list = new ArrayList<Bank>();
		list.add(bank1);
		list.add(bank2);
		list.add(bank3);
		return list;
	}

}
