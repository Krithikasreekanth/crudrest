package com.model;

import java.io.Serializable;

public class Bank implements Serializable {
	private int bankId;
	private String bankName;
	
	private String bankCity;
	
	//constructor==to initialise values to variables.
	public Bank() {
		super();
	}
/*public Bank(int bankId, String bankName, String bankCity) {
		// TODO Auto-generated constructor stub
	}*/
public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCity() {
		return bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}
	public Bank(int bankId, String bankName, String bankCity) {
		super();
		this.bankId = bankId;
		this.bankName = bankName;
		this.bankCity = bankCity;
	}
	

}
