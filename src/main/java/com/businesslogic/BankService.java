package com.businesslogic;

import java.util.List;

import com.model.Bank;

public interface BankService {
	public boolean createService(Bank bank);

	public Bank readService(int bankId);

	public String updateService(String bankCity);

	public boolean deleteService(int bankId);

	public List<Bank> getAllBankService();
	
}
