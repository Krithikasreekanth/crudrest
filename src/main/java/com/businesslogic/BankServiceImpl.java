package com.businesslogic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cruddao.BankDao;
import com.model.Bank;
@Service
public class BankServiceImpl implements BankService {
	@Autowired
	private BankDao bankDao;

	@Override
	public boolean createService(Bank bank) {
		

		return bankDao.create(bank);
	}

	@Override
	public Bank readService(int bankId) {
		
		return bankDao.read(bankId);
	}

	@Override
	public String updateService(String bankCity) {
		// TODO Auto-generated method stub
		return bankDao.update(bankCity);
	}

	@Override
	public boolean deleteService(int bankId) {
		// TODO Auto-generated method stub
		return bankDao.delete(bankId);
	}

	@Override
	public List<Bank> getAllBankService() {
		// TODO Auto-generated method stub
		return bankDao.getAllBank();
	}

}
